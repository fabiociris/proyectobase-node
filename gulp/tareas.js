"use strict";

var gulp = require( "gulp" );

require( "./otros/tareas.js" );
gulp.task( "js:hint", require( "./jshint" ) );
gulp.task( "js:inspect", require( "./inspect" ) );
gulp.task( "js:complexity", require( "./complejidad.js" ) );
gulp.task( "js:cs", require( "./jscs.js" ).src );
gulp.task( "js:qa", [ "js:hint", "js:cs", "js:complexity", "js:inspect" ], function( cb ) {
  return cb();
} );

gulp.task( "help", require( "./ayuda.js" ) );

gulp.task( "serve", [ "js:qa" ], require( "./servidor" ) );
gulp.task( "default", [ "serve" ] );

gulp.task( "generar", require( "./generador.js" ) );
