"use strict";

var gulp = require( "gulp" );
var jscs = require( "gulp-jscs" );
var rutas = require( "./rutas" );
var gutil = require( "gulp-util" );
var plumber = require( "gulp-plumber" );
var notify = require( "gulp-notify" );

exports.src = src;

function src() {
  return gulp.src( rutas.scripts.watch )
    .pipe( plumber( {
      errorHandler: notify.onError( "Error: <%= error.message %>" )
    } ) )
    .pipe( jscs( {fix: true} ) )
    .pipe( jscs.reporter() )
    .pipe( gulp.dest( "src" ) )
    .on( "error", gutil.log );
}
