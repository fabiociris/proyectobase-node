"use strict";

var estaAutorizado = require( "../../src/login/middleware.js" ).estaAutorizado;
var httpMocks = require( "node-mocks-http" );
var expec = require( "chai" ).expect;

describe( "Función está autorizado", function() {

  it( "EL request no tiene un header de autorización", sinAuth );
  it( "EL request tiene un header de autorización", sigueUsuario );
  it( "EL request tiene un header de autorización inválido", invalido );

  function sinAuth() {
    var request = httpMocks.createRequest( {
      method: "GET",
      url: "/"
    } );
    var response = httpMocks.createResponse();
    estaAutorizado( request, response );
    var respuesta = "El request no tiene un encabezado de Autorización";
    expec( response._getData() ).to.equal( respuesta );
    expec( response.statusCode ).to.equal( 401 );
  }

  function sigueUsuario() {
    var request = httpMocks.createRequest( {
      method: "GET",
      url: "/",
      headers: {
          authorization: require( "../token.js" )
        }
    } );
    var response = httpMocks.createResponse();
    estaAutorizado( request, response, function() {} );
    expec( response.statusCode ).to.equal( 200 );
  }

  function invalido() {
    var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlhdCI6MTQ0OTA3Nzc" +
    "2NSwiZXhwIjozMDI3MDAwOTY1fQ.yH_57waXkQIwIv9_tqv";
    var request = httpMocks.createRequest( {
      method: "GET",
      url: "/",
      headers: {
          authorization: "Bearer " + token
        }
    } );
    var response = httpMocks.createResponse();
    var res = "Signature verification failed";
    estaAutorizado( request, response, function() {} );
    expec( response._getData() ).to.equal( res );
    expec( response.statusCode ).to.equal( 401 );
  }
} );
