"use strict";

var expect = require( "chai" ).expect;
var origin = require( "../../src/commons/origin.js" );

describe( "Función de orígenes", function() {

  it( "Devuelve 1 string cuando recibe un string", respuestaSimple );
  it( "Devuelve un array de strings cuando recibe un string separado por commas", respuestaArray );

  function respuestaSimple() {
    var string = "http://ciriscr.com";
    expect( origin( string ) ).to.equal( string );
  }

  function respuestaArray() {
    var string = "http://ciriscr.com, http://galeniscr.com";
    var resp = [ "http://ciriscr.com", "http://galeniscr.com" ];
    expect( origin( string ) ).to.eql( resp );
  }
} );
