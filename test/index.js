"use strict";

var request = require( "supertest" );
require( "../src/bd.js" );

describe( "Servidor completo - ejemplo de test", function() {
  var server;
  beforeEach( function() {
    server = require( "../src/index.js" );
  } );

  it( "Responde a /", respuestaRoot );

  function respuestaRoot( done ) {
    request( server )
      .get( "/" )
      .expect( 200, done );
  }
} );
