"use strict";

var express = require( "express" );
var cors = require( "cors" );
var app = express();
var bodyParser = require( "body-parser" );
var Log = require( "log" );
var config = require( "./config.js" );

var log = new Log( "debug" );
var corsOptions = {
  origin: require( "./commons/origin.js" )( config.ORIGIN ),
  credentials: true
};
app.options( "*", cors( corsOptions ) );
app.use( cors( corsOptions ) );
log.debug( "Configurando el servidor" );
app.use( bodyParser.urlencoded( {
  extended: true
} ) );
app.use( bodyParser.json() );

/***************** ROUTER **********************/
var middleware = require( "./login/middleware.js" );
var estaAutorizado = middleware.estaAutorizado;

var loginGoogle = require( "./login/google.js" );
app.use( "/api/login", loginGoogle );

var loginOdoo = require( "./login/odoo.js" );
app.use( "/api/login", loginOdoo );

var usuarios = require( "./rutas/usuario.js" );
app.use( "/api/usuario", estaAutorizado, usuarios );

app.get( "/", function( req, res ) {
  return res.status( 200 ).end( "El API está funcionando correctamente" );
} );

/**************** Rutas globales  **********************/

app.use( function( req, res ) {
  res.status( 404 ).send( "Recurso no encontrado." );
} );

app.use( function( err, req, res ) {
  console.error( err.stack );
  res.status( 500 ).send( "Error del servidor" );
} );

/****************  Iniciar el servidor *************/

var puerto = config.PUERTO;
log.debug( "Cargando conexiones a MongoDB" );
require( "./bd.js" ); //cargar mondo
app.listen( puerto );
log.info( "Servidor iniciado en puerto ", puerto );
module.exports = app;
