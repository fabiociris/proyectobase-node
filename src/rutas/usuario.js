"use strict";

var express = require( "express" );

//var Log = require("log");
var _ = require( "lodash" );

//var log = new Log("debug");

var router = express.Router();
var Usuario = require( "../modelos/usuario.js" );

function uno( res ) {
  return function( id ) {
    Usuario.findOne( {"_id": id}, function( err, obj ) {
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      if ( _.isEmpty( obj ) ) {
        return res.status( 404 ).end();
      }
      return res.json( obj );
    } );
  };
}

router.get( "/yo", function( req, res ) {
  uno( res )( req.usuario );
} );

router.get( "/:id", function( req, res ) {
  uno( res )( req.params.id );
} );

router.get( "/", function( req, res ) {
  var query = {nombre: {"$regex": req.query.texto, $options: "i"}};
  var params = {skip: req.query.pagina, limit: req.query.cantidad};
  Usuario.find( query, null, params, function( err, obj ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    return res.json( obj );
  } );
} );

module.exports = router;
