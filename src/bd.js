"use strict";
var Log = require( "log" );
var mongoose = require( "mongoose" );

var log = new Log( "debug" );
mongoose.connect( require( "./config.js" ).MONGO_URI );

var db = mongoose.connection;
db.on( "error", function( error ) {
  log.error( "Error de conexión:" + error );
} );
db.once( "open", function() {
  log.info( "Conexión establecida" );
} );

module.exports = db;
