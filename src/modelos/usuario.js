"use strict";

var mongoose = require( "mongoose" );
var esquema = new mongoose.Schema( {
  correo: { type: String, unique: true },
  password: { type: String, select: false },
  odooId: {type: String, unique: true},
  nombre: {type: String, required: true},
  foto: String,
  google: String
} );

var Usuario = mongoose.model( "Usuario", esquema );

module.exports = Usuario;
