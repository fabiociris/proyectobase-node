"use strict";
var _ = require( "lodash" );

module.exports = Comunes;

function Comunes( modelo ) {
  this.modelo = modelo;
}

Comunes.prototype.findOne = wrapperFO;
Comunes.prototype.findOneAndUpdate = putID;
Comunes.prototype.contar = contar;
Comunes.prototype.skipLimitABS = skipLimitABS;

function wrapperFO( req, res ) {
  var este = this;
  var id = req.params.id;
  return function findOne( resp ) {
    este.modelo.findOne( {"_id": id, "borrado": false}, function( err, obj ) {
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      if ( _.isEmpty( obj ) ) {
        return res.status( 404 ).end();
      }
      return resp( res, obj );
    } );
  };
}

function putID( req, res ) {
  var este = this;
  var id = req.params.id;
  return function interna() {
    var query = {"_id": id, "borrado": false};
    var opciones = {multi: false, upsert: false, new: true, runValidators: true };
    este.modelo.findOneAndUpdate( query, req.body, opciones, function( err, obj ) {
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      if ( _.isEmpty( obj ) ) {
        return res.status( 404 ).end();
      }
      return res.json( obj );
    } );
  };
}

function contar( res, query, docs ) {
  query.exec( function( errorCont, resul ) {
    if ( errorCont ) {
      return res.status( 500 ).send( errorCont );
    } else {
      return res.json( {"docs": docs, "contador": resul} );
    }
  } );
}

function skipLimitABS( query ) {
  var cantidad = estabilizar( query.cantidad );
  var total = estabilizar( query.pagina ) * cantidad;
  return res( cantidad, total );
}

function res( cant, total ) {
  return {
    cantidad: cant,
    total: total
  };
}

function estabilizar( param ) {
  return parseInt( param || 0 );
}
