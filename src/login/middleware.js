"use strict";
var jwt = require( "jwt-simple" );
var moment = require( "moment" );
var config = require( "../config.js" );

exports.estaAutorizado = estaAutorizado;
exports.crearJWT = crearJWT;

function estaAutorizado( req, res, next ) {
  if ( !req.headers.authorization ) {
    var resp = "El request no tiene un encabezado de Autorización";
    return res.status( 401 ).send( resp );
  }
  validar( req, res );
  next();
}

function validar( req, res ) {
  try {
    var payload = decodificar( token( req ) );
    if ( payload.exp <= moment().unix() ) {
      return res.status( 401 ).send( "Token expirado" );
    }
    req.usuario = payload.sub;
  } catch ( err ) {
    return res.status( 401 ).send( err.message );
  }
}

function token( req ) {
  return req.headers.authorization.split( " " )[1];
}

function decodificar( token ) {
  return jwt.decode( token, config.TOKEN_SECRET );
}

function crearJWT( usuario ) {
  var payload = {
    sub: usuario._id,
    iat: moment().unix(),
    exp: moment().add( 1, "years" ).unix()
  };
  return jwt.encode( payload, config.TOKEN_SECRET );
}
