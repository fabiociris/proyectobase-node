"use strict";

var express = require( "express" );
var Log = require( "log" );
var config = require( "../config.js" );
var request = require( "request" );
var jwt = require( "jwt-simple" );
var middleware = require( "./middleware.js" );
var Usuario = require( "../modelos/usuario.js" );
var log = new Log( "debug" );
var router = express.Router();

function resultadoUsuario( res, profile ) {
  return function res( err, user ) {
    if ( !user ) {
      return res.status( 500 ).send( { message: "El usuario no existe" } );
    }
    setUsuario( user );
  };
  function setUsuario( user ) {
    user.google = profile.sub;
    user.picture = user.picture || profile.picture.replace( "sz=50", "sz=200" );
    user.displayName = user.displayName || profile.name;
    user.save( function() {
      var token = middleware.crearJWT( user );
      res.send( { token: token } );
    } );
  }
}

router.post( "/google", function( req, res ) {
  log.debug( "Logueando con google" );
  var accessTokenUrl = "https://accounts.google.com/o/oauth2/token";
  var peopleApiUrl = "https://www.googleapis.com/plus/v1/people/me/openIdConnect";
  var params = {
    code: req.body.code,
    "client_id": req.body.clientId,
    "client_secret": config.GOOGLE_SECRET,
    "redirect_uri": req.body.redirectUri,
    "grant_type": "authorization_code"
  };

  // Step 1. Exchange authorization code for access token.
  request.post( accessTokenUrl, { json: true, form: params }, function( err, response, token ) {
    var s = "access_token";
    var headers = { Authorization: "Bearer " + token[s] };

    // Step 2. Retrieve profile information about the current user.
    var query = { url: peopleApiUrl, headers: headers, json: true };
    request.get( query, function( err, response, profile ) {
      if ( profile.error ) {
        return res.status( 500 ).send( {message: profile.error.message} );
      }

      // Step 3a. Link user accounts.
      if ( req.headers.authorization ) {
        Usuario.findOne( { google: profile.sub }, function( err, existingUser ) {
          if ( existingUser ) {
            var resp = { message: "There is already a Google account that belongs to you" };
            return res.status( 409 ).send( resp );
          }
          var token = req.headers.authorization.split( " " )[1];
          var payload = jwt.decode( token, config.TOKEN_SECRET );
          Usuario.findById( payload.sub, resultadoUsuario );
        } );
      } else {

        // Step 3b. Create a new user account or return an existing one.
        Usuario.findOne( { google: profile.sub }, function( err, obj ) {

          if ( obj ) {
            return res.send( { token: middleware.crearJWT( obj ) } );
          }
          var user = new Usuario();
          user.google = profile.sub;
          user.foto = profile.picture.replace( "sz=50", "sz=200" );
          user.nombre = profile.name;
          user.correo = profile.email;
          user.save( function() {
            var token = middleware.crearJWT( user );
            res.send( { token: token } );
          } );
        } );
      }
    } );
  } );
} );

module.exports = router;
