"use strict";

var express = require( "express" );
var config = require( "../config.js" );
var middleware = require( "./middleware.js" );
var Usuario = require( "../modelos/usuario.js" );
var http = require( "http" );
var router = express.Router();
var Log = require( "log" );
var log = new Log( "debug" );

router.post( "/odoo", function( req, res ) {
  return loginOdoo( req, res );
} );

function opciones( path, longitud, sessionId ) {
  var headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Content-Length": longitud
  };
  if ( sessionId ) {
    headers.Cookie = "session_id=" + sessionId;
  }
  return {
    host: config.ODOO_HOST,
    port: config.ODOO_PORT,
    path: path,
    method: "POST",
    headers: headers
  };
}

function buscarUsuario( res, respuestaFinal ) {
  Usuario.findOne( {
    odooId: respuestaFinal.result.uid
  }, function( err, obj ) {
    if ( obj ) {
      log.debug( "Usuario encontrado" );
      var s = "session_id";
      return res.send( generarRespuesta( obj, respuestaFinal.result[s] ) );
    } else {
      log.debug( "Primer logueo del usuario. Generando registro." );
      return nuevoUsuario( respuestaFinal.result, res );
    }
  } );
}

function loginOdoo( req, res ) {
  var json = paramsOdoo( req.body.login, req.body.password );
  var opt = opciones( "/web/session/authenticate", json.length );
  var conectar = http.request( opt, function( result ) {
    var respuestaOdoo = "";
    result.setEncoding( "utf8" );
    result.on( "data", function( chunk ) {
      respuestaOdoo += chunk;
    } );
    result.on( "end", function end() {
      var respuestaFinal = JSON.parse( respuestaOdoo );
      if ( respuestaFinal.error || !respuestaFinal.result.uid ) {
        return res.status( 401 ).send( respuestaFinal );
      }
      buscarUsuario( res, respuestaFinal );
    } );
  } );
  conectar.write( json );
}

function nuevoUsuario( resultadoLogin, res ) {
  var json = paramsUsuario( resultadoLogin.uid );
  var s = "session_id";
  var opt = opciones( "/web/dataset/call_kw", json.length, resultadoLogin[s] );
  var consultarUsuario = http.request( opt, function( result ) {
    var response = "";
    result.setEncoding( "utf8" );
    result.on( "data", function( chunk ) {
      response += chunk;
    } );

    result.on( "end", function() {
      var respuestaFinal = JSON.parse( response );
      var usuarioOdoo = respuestaFinal.result;
      var user = new Usuario();
      var i = "image_small";
      user.nombre = usuarioOdoo.name;
      user.correo = resultadoLogin.username;
      user.odooId = usuarioOdoo.id;
      user.foto = usuarioOdoo[i];
      user.save( function() {
        return res.send( generarRespuesta( user, resultadoLogin[s] ) );
      } );
    } );
  } );
  consultarUsuario.write( json );
}

function generarRespuesta( usuario, sesionOdoo ) {
  function resp() {
    return {
      nombre: usuario.nombre,
      foto: usuario.foto,
      sesionOdoo: sesionOdoo,
      token: token
    };
  }
  var token = middleware.crearJWT( usuario );
  return {
    token: token,
    usuario: resp()
  };
}

function paramsOdoo( login, pass ) {
  var obj = {
    db: config.ODOO_DB,
    login: login,
    password: pass
  };

  return JSON.stringify( {
    params: obj
  } );
}

function paramsUsuario( uid ) {
  var obj = {
    "model": "res.users",
    "method": "read",
    "args": [ uid, [ "name", "image_small" ] ],
    "kwargs": {}
  };
  return JSON.stringify( {
    params: obj
  } );
}

module.exports = router;
