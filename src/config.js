"use strict";

var db = "mongodb://base:base@ds049854.mongolab.com:49854/proyecto-base";

module.exports = {
  TOKEN_SECRET: process.env.TOKEN_SECRET || "Sunlight Yellow Overdrive",
  MONGO_URI: process.env.MONGO_URI || db,
  PUERTO: process.env.PUERTO || 3001,
  ORIGIN: process.env.ORIGIN || "http://localhost:3000",
  ODOO_HOST: process.env.ODOO_HOST || "odoo.ciriscr.com",
  ODOO_PORT: process.env.ODOO_PORT || 8069,
  ODOO_DB: process.env.ODOO_DB || "odoo",
  GOOGLE_SECRET: process.env.GOOGLE_SECRET || "QG0DW_bKAjGBC73DXpYwMYFL"
};
